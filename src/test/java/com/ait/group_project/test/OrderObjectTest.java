package com.ait.group_project.test;





import static org.junit.Assert.assertEquals;

import  org.junit.Test;

import com.ait.librarySystem.model.Book;
import com.ait.librarySystem.model.Orders;

public class OrderObjectTest {

	@Test
	public void setOrderDetails()
	{
		Orders o = new Orders();
		o.setBookId(1);
		o.setBookName("Frankenstein");
		o.setOrderStatus("Shipped");
		o.setUserName("Yugi");
		
		assertEquals(o.getBookId(), 1);
		assertEquals(o.getBookName(), "Frankenstein");
		assertEquals(o.getOrderStatus(), "Shipped");
		assertEquals(o.getUserName(), "Yugi");
	}
}
