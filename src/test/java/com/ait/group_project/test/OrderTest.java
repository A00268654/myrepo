package com.ait.group_project.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ait.librarySystem.model.Book;
import com.ait.librarySystem.model.Orders;

public class OrderTest {

	@Test
	public void pipeline()
	{
		Orders o1 = new Orders();
		Orders o2 = new Orders();
		
		o1.setId(1);
		o2.setId(1);
		assertEquals(o1.getId(), o2.getId());
	}
}
