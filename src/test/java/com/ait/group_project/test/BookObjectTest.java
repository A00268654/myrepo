package com.ait.group_project.test;

import static org.junit.Assert.assertEquals;

import  org.junit.Test;
import com.ait.librarySystem.model.Book;

public class BookObjectTest {

	@Test
	public void setBookDetails()
	{
		//Test book getters and Setters.
		Book b = new Book();
		b.setAuthor("Johnny");
		b.setDescription("HelloWorld");
		b.setId(1);
		b.setName("Jimmy");
		b.setBookStatus("Available");
		
		assertEquals(b.getAuthor(), "Johnny");
		assertEquals(b.getDescription(), "HelloWorld");
		assertEquals(b.getId(), 1);
		assertEquals(b.getBookName(), "Jimmy");
	}
}
