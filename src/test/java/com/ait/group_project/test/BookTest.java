package com.ait.group_project.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ait.librarySystem.model.Book;

public class BookTest {
	
	@Test
	public void pipeline()
	{
		//Tests
		Book b1 = new Book();
		Book b2 = new Book();
		
		b1.setAuthor("Jim");
		b2.setAuthor("Jim");
		assertEquals(b1.getAuthor(),b2.getAuthor());
	}
}
