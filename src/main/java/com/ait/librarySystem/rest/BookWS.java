package com.ait.librarySystem.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;

import java.util.List;
import javax.ws.rs.core.Response;
import javax.ejb.EJB;

import com.ait.librarySystem.model.Book;
import com.ait.librarySystem.model.Orders;
import com.ait.librarySystem.service.BookDAO;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

@Path("/books")
@Stateless
@LocalBean
public class BookWS
{
    @EJB
    private BookDAO bookDao;
    
    @GET
    @Produces({ "application/json" })
    public Response findAll() {
        System.out.println("Get all books");
        final List<Book> books = (List<Book>)this.bookDao.getAllBooks();
        System.out.println("got books");
        System.out.println(books.size());
        return Response.status(200).entity((Object)books).build();
    }
    
    @GET
    @Produces({ "application/json" })
    @Path("/{id}")
    public Response findBookById(@PathParam("id") final int id) {
        final Book book = this.bookDao.getBook(id);
        return Response.status(200).entity((Object)book).build();
    }
    
    @POST
    @Consumes({ "application/json","application/xml" })
    @Produces({ "application/json","application/xml" })
    public Book addBook(final Book book) {
        return this.bookDao.addBook(book);
    }
    
    @PUT
    @Path("/update/{id}")
    @Consumes({ "application/json" })
    public Response updateBook(final Book book) {
        this.bookDao.update(book);
        return Response.status(204).entity(book).build();
    }
    
    @GET
    @Path("search/{query}")
    @Produces({ "application/json", "application/xml" })
    public Response findByAuthor(@PathParam("query") final String author) {
    	List<Book> book = this.bookDao.getBooksByAuthor(author);
        return Response.status(201).entity(book).build();
    }

    @DELETE
    @Path("/delete/{id}")
    public Response deleteBook(@PathParam("id") int id)
    {
    	bookDao.delete(id);
    	return Response.status(204).build();
    }
}