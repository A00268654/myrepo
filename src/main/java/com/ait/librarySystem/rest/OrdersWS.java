package com.ait.librarySystem.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.Application;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.ait.librarySystem.model.Book;
import com.ait.librarySystem.model.Orders;
import com.ait.librarySystem.model.User;
import com.ait.librarySystem.service.OrdersDAO;
import com.google.common.net.MediaType;

import cucumber.api.java.ast.Daos;

@Path("/orders")
@Stateless
@LocalBean
public class OrdersWS {
	
	  	@EJB
	    private OrdersDAO orderDao;
	    
	    @GET
	    @Produces({ "application/json" })
	    public Response getAllOrders() {
	        final List<Orders> orders = (List<Orders>)this.orderDao.getAllOrders();
	        return Response.status(200).entity((Object)orders).build();
	    }
	    @POST
	    @Consumes({"application/json","application/xml"})
	    @Produces({"application/json","application/xml"})
	    public Orders addOrder(final Orders order) {
	        return this.orderDao.addOrder(order);
	    }
	    @GET
	    @Produces({ "application/json" })
	    @Path("/{userName}")
	    public Response findOrderByUserName(@PathParam("userName") final String username) {
	        final List<Orders> order = this.orderDao.getOrderByUserName(username);
	        return Response.status(201).entity((Object)order).build();
	    }
	    
	    @GET
	    @Produces({ "application/json" })
	    @Path("/orderID/{id}")
	    public Response findOrderById(@PathParam("id") final int id) {
	        final Orders order = this.orderDao.getOrder(id);
	        return Response.status(201).entity((Object)order).build();
	    }
	    @PUT
	    @Path("/update/{id}")
	    @Consumes({ "application/json" })
	    public Response updateOrder(final Orders order) {
	        this.orderDao.update(order);
	        return Response.status(200).entity(order).build();
	    }
	    @DELETE
	    @Path("/delete/{id}")
	    public Response deleteOrder(@PathParam("id") int id)
	    {
	    	orderDao.delete(id);
	    	return Response.status(204).build();
	    }
}
