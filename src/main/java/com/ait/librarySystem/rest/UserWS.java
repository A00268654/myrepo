package com.ait.librarySystem.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.ait.librarySystem.model.Book;
import com.ait.librarySystem.model.Orders;
import com.ait.librarySystem.model.User;
import com.ait.librarySystem.service.UserDAO;

@Path("/users")
@Stateless
@LocalBean
public class UserWS {
	 	@EJB
	    private UserDAO userDao;
	 	
	    @GET
	    @Produces({ "application/json" })
	    public Response getAllUsers() {
	        final List<User> users = (List<User>)this.userDao.getAllUsers();
	        return Response.status(200).entity((Object)users).build();
	    }
	    @GET
	    @Produces({ "application/json" })
	    @Path("/{userName}")
	    public Response findByName(@PathParam("userName") final String username) {
	        final List<User> user = this.userDao.getUserByUserName(username);
	        return Response.status(200).entity((Object)user).build();
	    }

}
