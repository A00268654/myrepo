package com.ait.librarySystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="orders")
@XmlRootElement
public class Orders {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private int id;
	 private int bookId;
	 private String userName;
	 private String bookName;
	 private String pricePerWeek;
	 private String orderStatus;
	 
	public Orders()
	{
		 this.id = 0;
		 this.bookId = 0;
		 this.userName = "";
		 this.bookName = "";
		 this.pricePerWeek = "";
		 this.orderStatus = "";
	}
		
	public Orders(int id, int bookId, String username, String bookname, String pricePerWeek, String orderStatus)
	{
		this.id = id;
		this.bookId = bookId;
		this.userName = username;
		this.bookName = bookname;
		this.pricePerWeek = pricePerWeek;
		this.orderStatus = orderStatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getPricePerWeek() {
		return pricePerWeek;
	}
	public void setPricePerWeek(String pricePerWeek) {
		this.pricePerWeek = pricePerWeek;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	 


}
