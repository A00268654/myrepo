package com.ait.librarySystem.model;

import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Entity;

@Entity
@XmlRootElement
public class Book
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String bookName;
    private String image;
    private String price;
    private String author;
    private String bookStatus;
    private String description;
    
    public Book()
    {
    	this.id = 0;
    	this.bookName = "";
    	this.image = "";
    	this.price = "";
    	this.author = "";
    	this.bookStatus = "";
    	this.description = "";
    }
    public Book(int id, String bookName, String image, String price, String author, String bookStatus, String description)
    {
    	this.id = id;
    	this.bookName = bookName;
    	this.image = image;
    	this.price = price;
    	this.author = author;
    	this.bookStatus = bookStatus;
    	this.description = description;
    }
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public String getBookName() {
        return this.bookName;
    }
    
    public void setName(final String bookName) {
        this.bookName = bookName;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(final String image) {
        this.image = image;
    }
    
    public String getPrice() {
        return this.price;
    }
    
    public void setPrice(final String price) {
        this.price = price;
    }
    
    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(final String author) {
        this.author = author;
    }
    
    public String getBookStatus() {
        return this.bookStatus;
    }
    
    public void setBookStatus(final String bookStatus) {
        this.bookStatus = bookStatus;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
}