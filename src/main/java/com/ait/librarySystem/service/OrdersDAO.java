package com.ait.librarySystem.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ait.librarySystem.model.Book;
import com.ait.librarySystem.model.Orders;
import com.ait.librarySystem.model.User;


@Stateless
@LocalBean
public class OrdersDAO {
	
	  @PersistenceContext
	  private EntityManager em;
	  
    public List<Orders> getAllOrders() {
    	final Query query = this.em.createQuery("SELECT o FROM Orders o");
        return (List<Orders>)query.getResultList();
    }
    
    public List<Orders> getOrderByUserName(final String username) {
        final Query query = this.em.createQuery("Select b from Orders b Where b.userName Like ?1");
        query.setParameter(1, (Object)("%" + username + "%"));
        return (List<Orders>)query.getResultList();
    }
    
    public Orders addOrder(final Orders order) {
    	Orders newOrder = order;
    	em.merge(newOrder);
	    return order;
    }
    public void update(final Orders order) {
        this.em.merge((Object)order);
    }
    public Orders getOrder(final int id) {
        return (Orders)this.em.find((Class)Orders.class, (Object)id);
    }
    public void delete(int id) {
        this.em.remove(getOrder(id));
    }
    public void clearTable() {
    		em.createNativeQuery("TRUNCATE TABLE orders").executeUpdate();
    }
}
