package com.ait.librarySystem.service;

import javax.persistence.Query;

import com.ait.librarySystem.model.User;

import java.util.List;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class UserDAO
{
    @PersistenceContext
    private EntityManager em;
    
    public List<User> getAllUsers() {
        final Query query = this.em.createQuery("SELECT u FROM User u");
        return (List<User>)query.getResultList();
    }
    
    public List<User> getUserByUserName(final String username) {
        final Query query = this.em.createQuery("Select u from User u Where u.username Like ?1");
        query.setParameter(1, (Object)("%" + username.toUpperCase() + "%"));
        return (List<User>)query.getResultList();
    }

}