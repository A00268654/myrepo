package com.ait.librarySystem.service;

import javax.persistence.Query;
import javax.ws.rs.core.Response;

import com.ait.librarySystem.model.Book;
import com.ait.librarySystem.model.Orders;

import java.util.List;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class BookDAO
{
    @PersistenceContext
    private EntityManager em;
    
    public List<Book> getAllBooks() {
        final Query query = this.em.createQuery("SELECT b FROM Book b");
        return (List<Book>)query.getResultList();
    }
    
    public Book getBook(final int id) {
        return (Book)this.em.find((Class)Book.class, (Object)id);
    }
    
    public Book addBook(final Book book) {
    	em.merge(book);
	    return book;
    }
    
    public void update(final Book book) {
        this.em.merge((Object)book);
    }
    
    public void delete(int id) {
        this.em.remove(getBook(id));
    }
    public List<Book> getBooksByAuthor(final String author) {
        final Query query = this.em.createQuery("Select b from Book b Where b.author Like ?1");
        query.setParameter(1, (Object)("%" + author.toUpperCase() + "%"));
        return (List<Book>)query.getResultList();
    }
    public void clearTable() {
		em.createNativeQuery("TRUNCATE TABLE book").executeUpdate();
    }
}
