var loginUrl = "http://localhost:8080/librarySystem/library/users";
var bookUrl = "http://localhost:8080/librarySystem/library/books";

var findAll = function() {
    $.ajax({
        type : 'GET',
        url : bookUrl,
        dataType : "json",
        success : bookList
    });
};
var bookList = function(books){
	//To clear the append to remove duplication
	$('.bookList').html('');
	
    $('.bookList').append('<div class="row top-buffer"  outline-style="dotted" >');
    $.each(books, function(index, book) {
    	if(book.bookStatus === "AVAILABLE")
    		{
		        $('.row').append(
		            '<div class = "col-sm-7 col-md-5 col-lg-4 top-buffer">'+ 
		            '<div class = "details">' +
		            '<img src = "resources/BookshopImages/'+ book.image+'" height="220" width="150" >' +
		            '<h3 class="h3 mb-2 text-gray-800">' + book.bookName + '</h3>' +
		            '<h2><a id = "' + book.price + ' href="#">' + book.price + '</a></h2>' +
		            '<a href="#">' +
		            '<span id="' + book.id+ '" class="btn btn-info fa-3x">Details</span>' +
		            '</a></div>'
		        );
    		}
    })
    $('#bookList').append('</div>');

};

var bookInfoModal = function(id) {
	$.ajax({
		type : 'GET',
		url : bookUrl+"/" + id,
		dataType : "json",
		success : function(books) {
			$('#pic').attr("src","resources/BookshopImages/"+books.image);
			$('#title').text(books.bookName);
			$('#author').text(books.author);
			$('#price').text(books.price);
			$('#description').text(books.description);
			$('#bookModal').modal('show');

		}
	});
};


$(document).ready(function() {
	findAll();
	$(document).on("click", ".btn-info", function() {
		bookInfoModal(this.id);
		console.log("clicked");
	});
});