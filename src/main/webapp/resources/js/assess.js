//Q1 XMLHttpRequest
/*
var xhr = new XMLHttpRequest();

var getJSONDataXHR = function(getBooksurl, jsonData) {

    
    xhr.open('GET', getBooksurl, true);
    xhr.responseType = 'json';
    
    xhr.onreadystatechange = function() {
    	
        if (xhr.status == 200) {
        	console.log("JSON Data: "+ JSON.stringify(xhr.response));
        	jsonData(xhr.response);
        }
    };
    
    xhr.send();
};

getJSONDataXHR('http://localhost:8080/librarySystem/library/books/',  function(data) {

        for(i in data)
        {
        	document.getElementById("table_body").innerHTML += '<tr><td>' + data[i].id 
        	+ '</td><td>'+ data[i].bookName + '</td><td>' + data[i].author+ '</td><td>' + 
        	data[i].price+ '</td><td>'+'</td><td>' + data[i].bookStatus+ '</td><td>';
        }
});*/

//Q2 Promises

/*fetch("http://localhost:8080/librarySystem/library/books/")
  .then(response =>  {
	  if (response.ok) {
		  return response.json();
	  } else {
		  throw new Error('Unable to retrieve a response');
	  }})
  .then(book => document.getElementById("table_body").innerHTML += populateTable(book))
  //Extra functionality using event listener and queryselector
  .then(document.querySelector("html").addEventListener('click',function(event) 
		{document.body.style.backgroundColor = "lightblue";}))
	//Error handling using Promises
  .catch(error => alert(error));

var populateTable = (books) => {
 var table ="";
 books.forEach(book => table += '<tr><td>' + book.id + '</td><td>'+ book.bookName + '</td><td>' + book.author + '</td><td>' + 
        book.price+ '</td><td>'+'</td><td>' + book.bookStatus+ '</td><td>');
 return table;
};*/

//Q3 Vue.js

