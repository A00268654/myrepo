var orderURL = "http://localhost:8080/librarySystem/library/orders"
var bookURL = "http://localhost:8080/librarySystem/library/books"
		
var getOrderByUserName = function(userName){
		$.ajax({
			type: 'GET',
			url: orderURL+"/"+userName,
			dataType: "json",
			success: renderOrderList
		});
};	

var renderOrderList = function(orderList){
	console.log("Render Orders");
	$.each(orderList, function(index, order){
		$('#order_table_body').append('<tr><td>' + order.id + '</td><td>'+order.userName+'</td><td>' + order.bookName + '</td><td>' 
		+ order.pricePerWeek+ '</td><td>' + order.orderStatus+ '</td><td>'+'<a href="#"><span id="' + order.id + '" class="fa fa-times-circle" title="Cancel Your Order"></a></td></tr>');
	});
	
}
var deleteOrder = function(id){
	$.ajax({
		type: 'GET',
		url: orderURL+"/orderID/"+id,
		dataType: "json",
		success: function(order){
				$.ajax({
					type: 'GET',
					url: bookURL+"/"+order.bookId,
					dataType: "json",
					success: function(bookreturned){
						console.log("BOOK"+JSON.stringify(bookreturned));
						bookreturned.bookStatus = "AVAILABLE";
						$.ajax({
							type : 'PUT',
							contentType: "application/json",
							url : bookURL +"/update/"+bookreturned.id,
							dataType:"json",
							data:JSON.stringify({
								id : bookreturned.id,
								bookName : bookreturned.bookName,
								image : bookreturned.image,
								price : bookreturned.price,
								author : bookreturned.author,
								bookStatus : bookreturned.bookStatus }),

						});
					}
						
				});
			}
		});
		
		$.ajax({
			type: 'DELETE',
			url: orderURL+"/delete/"+id,
			dataType: "json"
		});
		
		
};
$(document).ready(function(){
	getOrderByUserName(localStorage.getItem('currentUser'));
	
	$(document).on("click", '.fa-times-circle', function() {
		deleteOrder(this.id);
		setTimeout(function(){location.reload()}, 1000);
	
	});
});

