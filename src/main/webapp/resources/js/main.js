//var rootURL = "http://localhost:8080/group-project/ecm/";
var bookUrl = "http://localhost:8080/librarySystem/library/books";
var loginUrl = "http://localhost:8080/librarySystem/library/users";
var orderUrl = "http://localhost:8080/librarySystem/library/orders";


$(document).on("click", '#submit', function() {
	findByName(inputUsername);
});

var findByName = function(username) {
	console.log('findByName: ' + username.value);
	$.ajax({
		type : 'GET',
		url : loginUrl +"/" + username.value,
		dataType : "json",
		success : checkpassword
	});
};

var checkpassword = function(userList) {
	if (userList == '')
		{
			alert("Please enter valid user")
		}
	
	var password = $('#inputPassword').val();
	
	$.each(userList, function(index,user) {
		console.log("TYPEEEE"+user.type);
		localStorage.setItem('currentUser',user.username);
		
		if (user.password == password) {
			if(user.type == "customer")
			{	
				window.location.href = "http://localhost:8080/librarySystem/Shop.html"+"?";
			}
			else if(user.type == "admin")
			{
				window.location.href = "http://localhost:8080/librarySystem/tables.html";
			}
		}
		else {
			alert("WRONG PASSWORD");
		}
	});
}

var createOrder = function(id) {
	$.ajax({
		type : 'GET',
		url : bookUrl+"/" + id,
		dataType : "json",
		success : function(book) {
			console.log("BOOK"+book.author);
			console.log(localStorage.getItem('currentUser'));
			book.status = "UNAVAILABLE";
				
				var newOrder = {
						id:0,
						bookId: book.id,
						userName: localStorage.getItem('currentUser'),
						bookName: book.bookName,
						pricePerWeek: book.price,
						orderStatus: "RESERVED - READY FOR COLLECTION"
				}
				
				$.ajax({
					type : 'POST',
					url : orderUrl,
					data: JSON.stringify(newOrder),
					contentType: "application/json",
				});
				
				book.bookStatus = "UNAVAILABLE";
				$.ajax({
					type : 'PUT',
					contentType: "application/json",
					url : bookUrl +"/update/"+book.id,
					dataType:"json",
					data:JSON.stringify({
						id : book.id,
						bookName : book.bookName,
						image : book.image,
						price : book.price,
						author : book.author,
						bookStatus : book.bookStatus }),

				});
				
				window.location.reload();
		}
	});
};

$(document).ready(function(){
	var selectedBook;
	$(document).on("click", '.btn-info', function() {
		selectedBook = this.id;
	});
	$(document).on("click", '.btn-success', function() {
		createOrder(selectedBook);
	});


});
