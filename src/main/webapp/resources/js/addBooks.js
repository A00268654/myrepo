var bookURL = "http://localhost:8080/librarySystem/library/books"
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

var findAll = function(){
	console.log("findAll");
		$.ajax({
			type: 'GET',
			url: bookURL,
			dataType: "json",
			success: renderList
	});
};	
	


		
var renderList = function(bookList){
	console.log("renderList");
	$.each(bookList, function(index, book){
		$('#table_body').append('<tr><td>' + book.bookName + '</td><td> <img src = "resources/BookshopImages/' +
		book.image + '" width = 70 height =100 /></td><td>' + book.price + '</td><td>' + book.author+ '</td><td>' + book.bookStatus+ '</td><td>'+
		'<a href="#"><span id="' + book.id + '" class="fas fa-sync" title="Change availabilty of the book"></span></a></td><td><a href="#"><span id="' + book.id + '" class="fas fa-trash-alt" title="Delete the book"></a></td></tr>');
	});
}

	


var addBook = function() {
	
	var newBook = {
			id:0,
			bookName: $('#inputBookname').val() ,
			image:$('#inputImage').val(),
			price:$('#inputPrice').val(),
			author:$('#inputAuthor').val(),
			bookStatus: "AVAILABLE",
			description:$('#inputDescription').val()};
			console.log(JSON.stringify(newBook));
			
	$.ajax({
		type : 'POST',
		url : bookURL,
		data: JSON.stringify(newBook),
		contentType: "application/json",
		
		
	});
};

var getBookByIdUpdate = function(id){
	console.log("findByID" + id);
		$.ajax({
			type: 'GET',
			url: bookURL+"/"+id,
			dataType: "json",
			success: updateBook
		});
};	
var updateBook = function(book) {
	console.log("UPDATING")
	
	
	var status;
	if(book.bookStatus === 'AVAILABLE')
	{
		book.bookStatus = 'UNAVAILABLE'
	}
	else if(book.bookStatus === "UNAVAILABLE")
	{
		book.bookStatus = "AVAILABLE"
	}
	
	$.ajax({
		type : 'PUT',
		contentType: "application/json",
		url : bookURL +"/update/"+book.id,
		dataType:"json",
		data:JSON.stringify({
			id : book.id,
			bookName : book.bookName,
			image : book.image,
			price : book.price,
			author : book.author,
			bookStatus : book.bookStatus }),

	});
	
	window.location.reload();
};

var deleteBook = function(id){
	console.log("DELETE" + id);
		$.ajax({
			type: 'DELETE',
			url: bookURL+"/delete/"+id,
			dataType: "json",
			success: function(){
				$("#statusUpdate").text("BOOK DELETED SUCCESSFULLY");
				setTimeout(function(){window.location.reload()}, 3000);
			}
		});
};	


$(document).ready(function(){
	findAll();

	$(document).on("click", '#addToLib', function() {
		addBook();
	});
	$(document).on("click", '.fa-sync', function() {
		console.log("BUTTONCLICKED")
		getBookByIdUpdate(this.id);
		
	});
	$(document).on("click", '.fa-trash-alt', function() {
		console.log("DELETECLICKED")
		deleteBook(this.id);
		
	});
});

