function logout() {
	$("#logout").click(function() {
		sessionStorage.clear();
		redirect();
	});
}

function redirect() {
	location.replace("login.xhtml");
}

$(document).ready(function() {
	logout();
});