var currentLocation = location.href;

function authCheck() {
	if (currentLocation == "http://localhost:8080/group-project/index.xhtml") {
		if (sessionStorage.getItem("userRole") != "ADMIN") {
			redirect();
		}
	} else if (currentLocation == "http://localhost:8080/Bar_Ordering_System/customer.html") {
		if (sessionStorage.getItem("userRole") != "CUSTOMER") {
			redirect();
		}
	} 
}

function redirect() {
	location.replace("login.xhtml")
}

$(document).ready(function() {
	authCheck();
});