var orderURL = "http://localhost:8080/librarySystem/library/orders";
var bookURL = "http://localhost:8080/librarySystem/library/books";
		
var getAllOrders = function(){
		$.ajax({
			type: 'GET',
			url: orderURL,
			dataType: "json",
			success: renderOrderList
		});
};	

var renderOrderList = function(orderList){
	$.each(orderList, function(index, order){
		var table;
		if(order.orderStatus === "RESERVED - READY FOR COLLECTION")
		{
			table = '#reserved_admin_orders';
		}
		if(order.orderStatus === "COLLECTED - LOAN PERIOD STARTED")
		{
			table = '#loaned_admin_orders';
		}
		if(order.orderStatus === "RETURNED - BOOK RETURNED TO THE LIBRARY")
		{
			table = '#returned_admin_orders';
		}
		$(table).append('<tr><td>' + order.id + '</td><td>'+order.userName+'</td><td>' + order.bookName + '</td><td>' 
		+ order.pricePerWeek+ '</td><td>' + order.orderStatus+ '</td><td>'+'<a href="#"><span id="' + order.id + '" class="fa fa-arrow-circle-down" title="Update status"></a></td>'
		+'<td><a href="#"><span id="' + order.id + '" class="fa fa-times-circle" title="Remove Order"></a></td></tr>');
	});
}
var deleteOrder = function(id){
	$.ajax({
		type: 'GET',
		url: orderURL+"/orderID/"+id,
		dataType: "json",
		success: function(order){
				$.ajax({
					type: 'GET',
					url: bookURL+"/"+order.bookId,
					dataType: "json",
					success: function(bookreturned){
						console.log("BOOK"+JSON.stringify(bookreturned));
						bookreturned.bookStatus = "AVAILABLE";
						$.ajax({
							type : 'PUT',
							contentType: "application/json",
							url : bookURL +"/update/"+bookreturned.id,
							dataType:"json",
							data:JSON.stringify({
								id : bookreturned.id,
								bookName : bookreturned.bookName,
								image : bookreturned.image,
								price : bookreturned.price,
								author : bookreturned.author,
								bookStatus : bookreturned.bookStatus }),

						});
					}
						
				});
		}
	});
		$.ajax({
			type: 'DELETE',
			url: orderURL+"/delete/"+id,
			dataType: "json"
		});
		
		
};
var updateOrder = function(id) {
	

	$.ajax({
		type: 'GET',
		url: orderURL+"/orderID/"+id,
		dataType: "json",
		success: function(order){
			console.log("UPDATING"+order.id);
			var status;
			if(order.orderStatus === 'RESERVED - READY FOR COLLECTION')
			{
				order.orderStatus = 'COLLECTED - LOAN PERIOD STARTED';
			}
			else if(order.orderStatus === "COLLECTED - LOAN PERIOD STARTED")
			{
				order.orderStatus = 'RETURNED - BOOK RETURNED TO THE LIBRARY';
			}
			
			$.ajax({
				type : 'PUT',
				contentType: "application/json",
				url : orderURL +"/update/"+order.id,
				dataType:"json",
				data:JSON.stringify({
					id : order.id,
					bookId : order.bookId,
					userName : order.userName,
					bookName : order.bookName,
					pricePerWeek : order.pricePerWeek,
					orderStatus : order.orderStatus })

			});
			
		}
	});
	
	
};
$(document).ready(function(){
	getAllOrders();
	
	$(document).on("click", '.fa-times-circle', function() {
		deleteOrder(this.id);
		setTimeout(function(){location.reload()}, 1000);
	
	});
	$(document).on("click", '.fa-arrow-circle-down', function() {
		console.log("ORDER UPDATEEEEE");
		updateOrder(this.id);
		setTimeout(function(){location.reload()}, 1000);
	
	});
});

