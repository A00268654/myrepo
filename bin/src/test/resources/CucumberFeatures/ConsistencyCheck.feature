Feature: ConsistencyCheck class

Scenario Outline: Event and Cause Code consistency check
When checking Event and Cause Code consistency event with <cause_code> <event_id>
Then event and cause code consistency check result should be <result>
Examples:
| cause_code | event_id | result |
|	0				 	 | 4097			| true	 |
| 1					 | 4098     | true   |
| 2          | 4125			| true	 |
| 3          | 4106     | true   |
| 4          | 4111     | false  |
| 5					 | 4112			| false	 |
| 6					 | 4113			| false  |
| 7					 | 4114			| false	 |

Scenario Outline: MCC and MNC consistency check
When checking MCC and MNC consistency with <MCC> <MNC>
Then MCC and MNC consistency check result should be <result>
Examples:
| MCC	| MNC	| result |
| 238	|	1		|	true   |
| 240	| 2		| true   |
| 302 | 36  | true   |
| 310 | 10  | true	 |
| 350 | 15  | false  |
| 400 | 20  | false  |
| 450 | 25  | false  |
| 500 | 30  | false  |