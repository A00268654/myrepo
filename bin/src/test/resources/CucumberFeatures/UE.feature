Feature: User Equipment Consistency Check
	As a system administrator
	I want to check call failure data for a valid TAC/UE Type
	So that only valid data is stored in database

Background:
Given the following userEquipmentReferenceTable data
|      tac   | 		manufacturer  	|
|     100100 |    Mitsubishi			|
|     100200 |    Siemens  				|
|     100300 |    Sony Ericsson   |
|     100400 |    Nokia   				|

Scenario Outline:  UE consistency check
When check incoming event with <tac>
Then result should be <result>
Examples:
|     tac    | 		 result	|
|     100100 |       true |
|     110011 |      false |
|     120100 |      false |
|     100400 |       true |
