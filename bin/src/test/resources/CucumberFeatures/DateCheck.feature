
@tag
Feature: check date  
Check if the date is in the correct format coming in from the excel document  
  

  @tag1
  Scenario: the test date/time should be sent to be checked 
    Given A date and time , a valid result should be given   
|	Date | result |
| 01/02/2020  01:01:01 | true		 	|
| 29/02/2020  01:01:01 | true	    |
| 01/02/2020  00:01:01 | true	  	| 
| 33/01/2020  01:01:01 | false  	|
| 01/13/2020  00:00:00 | false	 	|
| 33/01/2020  01:01:01 | false  	|
| 01/13/2020  00:00:00 | false	 	|
| 01/01/2020  25:01:01 | false	 	|
| 01/01/2020  01:60:01 | false	 	|
| 01/01/2020  01:01:60 | false	 	|
| 29/02/2021  01:01:01 | false	 	|
