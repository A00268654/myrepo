 Feature: Failure Class
	As a system administrator
	I want to have incoming records checked for a valid failure class
	So that only valid data is loaded into the system, 
	count of erroneous events is generated,
	 and details of erroneous events are logged in the system.
Background:
Given the following failureClassReferenceTable data
| failure_class | description |
|				0				| Some text1 	|
|				1				| Some text2	|
|				2				|	Some text3	|
|				3				|	Some text4	|
|				4				|	Some text5	|

Scenario Outline: Failure Class consistence check
When check incoming failure class with <failure_class>
Then result should be <result>
Examples:
| failure_class | result 			|
|			null		| true			 	|
|				19			| false				|
|				4				|	true				|
|				0				|	true				|
|				2				|	true				|
|				99			|	false				|
|				-1			|	false				|